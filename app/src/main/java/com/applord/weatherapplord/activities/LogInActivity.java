package com.applord.weatherapplord.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.applord.weatherapplord.R;
import com.applord.weatherapplord.utils.NetworkConnectionUtil;
import com.google.firebase.auth.FirebaseAuth;

public class LogInActivity extends AppCompatActivity {

    private Button btnLogin;
    private EditText txtEmail;
    private EditText txtPassword;
    private ProgressBar pbLogin;
    private String email, password;
    private FirebaseAuth firebaseAuth;
    private TextView lblForgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        //firebase initialise
        firebaseAuth = FirebaseAuth.getInstance();


        //if user already signed in
        if (firebaseAuth.getCurrentUser() != null)
        {
            Intent intent = new Intent(LogInActivity.this,MainActivity.class);
            startActivity(intent);
        }

        //UI initialization
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        pbLogin = findViewById(R.id.pbLogin);
        lblForgot = findViewById(R.id.lblForgot);

        //set progress bar to be invisible initially
        pbLogin.setVisibility(View.INVISIBLE);


        //handle login
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get email and password from textbox
                email = txtEmail.getText().toString().trim();
                password = txtPassword.getText().toString().trim();
                pbLogin.setVisibility(View.VISIBLE);

                boolean network =  NetworkConnectionUtil.isConnectedToInternet(LogInActivity.this);

                Toast.makeText(LogInActivity.this,"Connectivity: "+network ,Toast.LENGTH_SHORT).show();


                if (email.equals(""))
                {
                    pbLogin.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(),"Email cannot be empty",Toast.LENGTH_LONG).show();
                }
                else if (password.equals(""))
                {
                    pbLogin.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(),"Password cannot be empty",Toast.LENGTH_LONG).show();
                }
                else{


                    //Code unfinished
                }

            }
        });


    }
}
