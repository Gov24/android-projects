package com.applord.weatherapplord.models;

/**
 * Created by Ramulifho PG on 28/06/2019
 */
public class Wind {

    private float speed; //Speed
    private int deg; //Degree

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public int getDeg() {
        return deg;
    }

    public void setDeg(int deg) {
        this.deg = deg;
    }
}
