package com.applord.weatherapplord.api;

import com.applord.weatherapplord.models.CityWeather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Ramulifho PG on 29/06/2019
 */
public interface WeatherServicesAPIClient {

    @GET("forecast/daily")
    Call<CityWeather> getWeatherCity (@Query("q") String city, @Query("APPID")String key, @Query("units") String units , @Query("cnt") int days);

}
