package com.applord.weatherapplord.models;

import java.io.Serializable;

/**
 * Created By Ramulifho PG on 29/06/2019
 */
public class City  implements Serializable {

    private int id;  //City ID
    private String name;  //City Name
    private String country;  //Country code


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
