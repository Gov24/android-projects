package com.applord.weatherapplord.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ramulifho PG on 28/06/2019
 */
public class WeatherDetails implements Serializable {

    private int id;
    @SerializedName("main")
    private String shotDescription;
    @SerializedName("description")
    private String longDescription;
    private String icon;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShotDescription() {
        return shotDescription;
    }

    public void setShotDescription(String shotDescription) {
        this.shotDescription = shotDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
